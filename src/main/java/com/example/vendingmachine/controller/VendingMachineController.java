package com.example.vendingmachine.controller;

import com.example.vendingmachine.model.*;
import com.example.vendingmachine.repository.ProductRepository;
import com.example.vendingmachine.service.VendingMachineService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vending-machine")
public class VendingMachineController {
    private final VendingMachineService vendingMachineService;

    public VendingMachineController(VendingMachineService vendingMachineService) {
        this.vendingMachineService = vendingMachineService;
    }


    @PostMapping("/add-product")
    ResponseEntity<String> addProductToSlot(String productName, ProductType productType, Double productPrice, int productQuantity){

        if (productType != ProductType.COOKIE && productType != ProductType.CHIPS) {
            return ResponseEntity.badRequest().body("Invalid product type. Product type should be either COOKIE or CHIPS.");
        }
        if (productQuantity < 1 || productQuantity > 10) {
            return ResponseEntity.badRequest().body("A single slot can hold maximum 10 products. Range of quantity should be 1-10.");

        }

        // Add product to slot
        return vendingMachineService.addProductToSlot(productName,productType,productPrice,productQuantity);

    }

    @PostMapping("/remove-product")
    public ResponseEntity<String> removeProductFromSlot(@RequestParam long slotId) {
        boolean removed = vendingMachineService.removeProductFromSlot(slotId);
        if (removed) {
            return ResponseEntity.ok().body("Product removed from slot " + slotId);
        } else {
            return ResponseEntity.badRequest().body("Unable to remove product from slot " + slotId);
        }
    }

    @GetMapping("/stock")
    public ResponseEntity<String> getStock(@RequestParam int slotNumber) {
        if (slotNumber < 1 || slotNumber > 20) {
            return ResponseEntity.badRequest().body("Invalid slot number. Slot number should be between 1 and 20.");
        }
        int stock = vendingMachineService.getStock(slotNumber);

        return ResponseEntity.ok().body(String.valueOf(stock));
    }

    @GetMapping("/available-slots")
    public ResponseEntity<List<Integer>> getAvailableSlots() {
        List<Integer> availableSlots = vendingMachineService.getAvailableSlots();
        return ResponseEntity.ok().body(availableSlots);
    }

    @GetMapping("/all-slots")
    public List<Slot> getAllSlots() {
        return vendingMachineService.getAllSlots();
    }

    @GetMapping("/all-products")
    public List<Product> getAllProducts() {
        return vendingMachineService.getAllProducts();
    }

    @GetMapping("/slot")
    public ResponseEntity<Slot> getSlot(@RequestParam int slotNumber) {
        if (slotNumber < 1 || slotNumber > 20) {
            throw new IllegalArgumentException("Invalid slot number. Slot number should be between 1 and 20.");
        }
        return vendingMachineService.getSlot(slotNumber);
    }

    @PostMapping("/create-transaction")
    public ResponseEntity<String> getSlot(@RequestParam Long productId,@RequestParam int productQuantity,@RequestParam BigDecimal paymentPrice) {
        return vendingMachineService.createTransaction(productId,productQuantity,paymentPrice);
    }

    @GetMapping("/transaction")
    public List<Transaction> getTransaction() {
        return vendingMachineService.getTransactionList();
    }

    @DeleteMapping("/delete-product/{productId}")
    public ResponseEntity<String> removeProductById(@PathVariable Long productId) {
        boolean removed = vendingMachineService.removeProductById(productId);
        if (removed) {
            return ResponseEntity.ok().body("Product removed with id " + productId);
        } else {
            return ResponseEntity.badRequest().body("Unable to remove product with id " + productId);
        }
    }



}