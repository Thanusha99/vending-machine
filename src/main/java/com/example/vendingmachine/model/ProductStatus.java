package com.example.vendingmachine.model;

public enum ProductStatus {
    IN_STOCK,
    OUT_OF_STOCK

}
