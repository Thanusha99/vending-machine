package com.example.vendingmachine.model;

public enum ProductType {

    COOKIE,
    CHIPS

}
