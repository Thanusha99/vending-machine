package com.example.vendingmachine.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transactionId;

    @ManyToOne()
    @JoinColumn(name = "slot_id")
    private Slot slot;

    @Column(nullable = false)
    private BigDecimal amount;

    private String productName;

    private ProductType productType;

    private int productQuantity;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date transactionTime;

    public Transaction() {
    }

    public Transaction(Slot slot, BigDecimal amount, String productName, int productQuantity, Date transactionTime) {
        this.slot = slot;
        this.amount = amount;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.transactionTime = transactionTime;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }


}
