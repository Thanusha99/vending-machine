package com.example.vendingmachine.model;

import com.example.vendingmachine.repository.ProductRepository;
import com.example.vendingmachine.repository.SlotRepository;
import com.example.vendingmachine.service.VendingMachineService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "slot")
public class Slot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long slotId;

    @Column(nullable = false, unique = true)
    private int slotNumber;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ProductType productType;

    @Column(nullable = false)
    private int maxCapacity;
    @Column(nullable = false)
    private String slotSize;
    @Column(nullable = false)
    private int slotOccupancy;
    @OneToMany(mappedBy = "slot", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Product> products;

    private Long productId;

    public Slot() {
    }


    public Slot(int slotNumber, ProductType productType, int maxCapacity, String slotSize) {
        this.slotNumber = slotNumber;
        this.productType = productType;
        this.maxCapacity = maxCapacity;
        this.slotSize = slotSize;
        this.slotOccupancy = 0;
    }


    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void addProduct(Product product) {
        products.add(product);
        product.setSlot(this);
    }

//    public void removeProduct(Product product) {
//        products.remove(product);
//        product.setSlot(null);
//    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public int getSlotNumber() {
        return slotNumber;
    }

    public void setSlotNumber(int slotNumber) {
        this.slotNumber = slotNumber;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getSlotSize() {
        return slotSize;
    }

    public void setSlotSize(String slotSize) {
        this.slotSize = slotSize;
    }

    public int getSlotOccupancy() {
        return slotOccupancy;
    }

    public void setSlotOccupancy(int slotOccupancy) {
        this.slotOccupancy = slotOccupancy;
    }

    public boolean isFull() {
        return slotOccupancy == maxCapacity;
    }

    public boolean isEmpty() {
        return slotOccupancy == 0;
    }

    public boolean addProduct() {
        if (isFull()) {
            return false;
        }
        slotOccupancy++;
        return true;
    }

    @Transient
    private ProductRepository productRepository;

    public Slot(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transient
    private SlotRepository slotRepository;
    public Slot(SlotRepository slotRepository) {
        this.slotRepository = slotRepository;
    }

//    public boolean removeProduct(Product product) {
//        if (isEmpty()) {
//            return false;
//        }
//        slotOccupancy--;
//        if (slotOccupancy == 0) {
//            product.setProductStatus(ProductStatus.OUT_OF_STOCK);
//            productRepository.save(product);
//        }
//        return true;
//    }

    public boolean removeProduct() {
        if (isEmpty()) {
            return false;
        }
        slotOccupancy--;
        if (slotOccupancy == 0 && productId != null) {
            Product product = productRepository.findById(productId).orElseThrow();
            product.setProductStatus(ProductStatus.OUT_OF_STOCK);
            productRepository.save(product);
        }
        return true;
    }


//    public Product getProduct() {
//        return product;
//    }
//
//    public void setProduct(Product product) {
//        this.product = product;
//    }

//    @PostUpdate
//    public void checkAndUpdateProductStatus() {
//        if (this.slotOccupancy == 0) {
//            Product product = this.getProducts().get(0);
//            product.setProductStatus(ProductStatus.OUT_OF_STOCK);
//            productRepository.save(product);
//        }
//    }


    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}

