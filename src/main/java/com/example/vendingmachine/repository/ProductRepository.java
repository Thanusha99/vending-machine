package com.example.vendingmachine.repository;

import com.example.vendingmachine.model.Product;
import com.example.vendingmachine.model.ProductStatus;
import com.example.vendingmachine.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    Optional<Product> findByProductId(Long productId);

    @Modifying
    @Query("UPDATE Product p SET p.productStatus = :productStatus WHERE p.id = :productId")
    int updateProductStatus(@Param("productId") Long productId, @Param("productStatus") ProductStatus productStatus);

    //void updateProductStatus(Long productId, ProductStatus outOfStock);

//    @Modifying
//    @Query("UPDATE Product p SET p.productStatus = :productStatus WHERE p.slot.id = :slotId AND p.productStatus != 'OUT_OF_STOCK'")
//    int updateProductStatus(@Param("slotId") long slotId, @Param("productStatus") ProductStatus productStatus);


//    @Modifying
//    @Query("UPDATE Product p SET p.productStatus = :productStatus WHERE p.id = :productId")
//    void updateProductStatus(@Param("productId") Long productId, @Param("productStatus") ProductStatus productStatus);
}
