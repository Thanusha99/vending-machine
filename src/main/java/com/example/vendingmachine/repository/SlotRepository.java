package com.example.vendingmachine.repository;

import com.example.vendingmachine.model.ProductStatus;
import com.example.vendingmachine.model.ProductType;
import com.example.vendingmachine.model.Slot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SlotRepository extends JpaRepository<Slot, Long> {

    Slot findBySlotNumber(int slotNumber);

    Optional<Slot> findBySlotNumberAndProductType(int slotNumber, ProductType productType);

    @Query("SELECT s FROM Slot s WHERE s.productType = ?1 AND s.slotOccupancy < s.maxCapacity ORDER BY s.slotOccupancy ASC")
    List<Slot> findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(ProductType productType);

    @Modifying
    @Query("UPDATE Product p SET p.productStatus = 'OUT_OF_STOCK' WHERE p.slot.slotId = :slotId")
    void updateProductStatusWhenSlotOccupancyIsZero(@Param("slotId") Long slotId);

//    @Modifying
//    @Query("UPDATE Slot s SET s.slotOccupancy = 0 WHERE s.slotNumber = :slotNumber")
//    int setSlotOccupancyToZero(@Param("slotNumber") int slotNumber);
//
//    @Query("SELECT s FROM Slot s JOIN s.products p WHERE p.productId = :productId")
//    Slot findByProductId(@Param("productId") Long productId);


}
