package com.example.vendingmachine.repository;

import com.example.vendingmachine.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction,Long> {
}
