package com.example.vendingmachine.service;

import com.example.vendingmachine.model.*;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.List;

public interface VendingMachineService {

    ResponseEntity<String> addProductToSlot(String productName, ProductType productType, Double productPrice, int productQuantity);

    //boolean removeProductFromSlot(Long slotId);

    int getStock(int slotNumber);

    List<Integer> getAvailableSlots();

    List<Slot> getAllSlots();

    List<Product> getAllProducts();

    ResponseEntity<Slot> getSlot(int slotNumber);

    ResponseEntity<String> createTransaction(Long productId, int productQuantity, BigDecimal paymentPrice);

    List<Transaction> getTransactionList();

    boolean removeProductById(Long productId);

    boolean removeProductFromSlot(Long slotId);

    void updateProductStatusWhenSlotOccupancyIsZero(Long productId);
}
