package com.example.vendingmachine.service;

import com.example.vendingmachine.model.*;
import com.example.vendingmachine.repository.TransactionRepository;
import com.example.vendingmachine.repository.ProductRepository;
import com.example.vendingmachine.repository.SlotRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VendingMachineServiceImpl implements VendingMachineService {

    private final SlotRepository slotRepository;
    private final ProductRepository productRepository;
    private final TransactionRepository transactionRepository;
    private Slot[] slots;

    public VendingMachineServiceImpl(SlotRepository slotRepository, ProductRepository productRepository, TransactionRepository transactionRepository) {
        this.slotRepository = slotRepository;
        this.productRepository = productRepository;
        this.transactionRepository = transactionRepository;
        initializeSlots();
    }

    private void initializeSlots() {
        List<Slot> slotList = slotRepository.findAll();
        if (slotList.isEmpty()) {
            slots = new Slot[20];
            int cookieSlotCount = 0;
            int chipsSlotCount = 0;
            for (int i = 0; i < 20; i++) {
                if (cookieSlotCount < 10) {
                    slots[i] = new Slot(i + 1, ProductType.COOKIE, 10, SlotSize.S.toString());
                    cookieSlotCount++;
                } else {
                    slots[i] = new Slot(i + 1, ProductType.CHIPS, 10, SlotSize.M.toString());
                    chipsSlotCount++;
                }
            }
            slotRepository.saveAll(Arrays.asList(slots));
        } else {
            slots = slotList.toArray(new Slot[0]);
        }
    }

    public ResponseEntity<String> addProductToSlot(String productName, ProductType productType, Double productPrice, int productQuantity) {
        List<Slot> availableSlots = slotRepository.findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(productType);
        Slot slot = null;

        if (productType == ProductType.COOKIE) {
            // If product type is COOKIE, look for available slots of the same type first
            if (availableSlots.isEmpty()) {
                // If no slots are available, look for available slots of the CHIPS product type
                List<Slot> availableChipsSlots = slotRepository.findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(ProductType.CHIPS);
                if (productQuantity == 10) {
                    // If the product quantity is equal to the available size, use the first available cookie slot
                    List<Slot> availableCookieSlots = slotRepository.findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(ProductType.COOKIE);
                    var slotAny = availableCookieSlots.stream().findFirst();
                    if (slotAny.isPresent()) {
                        slot = slotAny.get();
                    } else if (availableChipsSlots.size() > 0) {
                        // If no cookie slots are available, use the first available chips slot
                        var slotChipsAny = availableChipsSlots.stream().findFirst();
                        if (slotChipsAny.isPresent()) {
                            slot = slotChipsAny.get();
                            productType = ProductType.CHIPS;
                        } else {
                            return ResponseEntity.badRequest().body("All slots for cookie and chips are full");
                        }
                    } else {
                        return ResponseEntity.badRequest().body("All slots for cookie are full");
                    }
                } else {
                    // If the product quantity is less than the available size, use the first available chips slot
                    var slotChipsAny = availableChipsSlots.stream().findFirst();
                    if (slotChipsAny.isPresent()) {
                        slot = slotChipsAny.get();
                        productType = ProductType.CHIPS;
                    } else {
                        return ResponseEntity.badRequest().body("All slots for cookie and chips are full");
                    }
                }
            } else {
                // If available slots of the COOKIE product type are found, use the first available slot
                slot = availableSlots.get(0);
            }
        } else {
            // If product type is CHIPS, look for available slots of the same type first
            if (availableSlots.isEmpty()) {
                // If no slots are available, return error message
                return ResponseEntity.badRequest().body("All slots for chips are full");
            } else {
                // If available slots of the CHIPS product type are found, use the first available slot
                slot = availableSlots.get(0);
            }
        }

        // Check if the slot has enough capacity for the new product quantity
        int totalStock = slot.getSlotOccupancy() + productQuantity;
        if (totalStock > 10) {
            // If the total stock exceeds the slot capacity, check if it's a cookie slot
            if (productType == ProductType.COOKIE) {
                // If it's a cookie slot, try to find a chips slot with enough capacity
                List<Slot> availableChipsSlots = slotRepository.findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(ProductType.CHIPS);
                if (!availableChipsSlots.isEmpty()) {
                    // If a chips slot with enough capacity is found, use it
                    Slot chipsSlot = availableChipsSlots.get(0);
                    int chipsCapacity = chipsSlot.getMaxCapacity() - chipsSlot.getSlotOccupancy();
                    if (productQuantity <= chipsCapacity) {
                        // If the chips slot has enough capacity, add the product to it
                        slot = chipsSlot;
                        totalStock = chipsSlot.getSlotOccupancy() + productQuantity;
                    } else {
                        // If the chips slot doesn't have enough capacity, return an error message
                        int availableSize = 10 - slot.getSlotOccupancy();
                        return ResponseEntity.badRequest().body("Available size in a slot is " + availableSize);
                    }
                } else {
                    // If no chips slot with enough capacity is found, return an error message
                    int availableSize = 10 - slot.getSlotOccupancy();
                    return ResponseEntity.badRequest().body("Available size in a slot is " + availableSize);
                }
            } else {
                // If it's not a cookie slot, return an error message
                int availableSize = 10 - slot.getSlotOccupancy();
                return ResponseEntity.badRequest().body("Available size in a slot is " + availableSize);
            }
        }


        // Add the new product to the slot and save the changes to the database
        slot.setSlotOccupancy(totalStock);
        Product product = new Product();
        product.setProductName(productName);
        product.setProductPrice(productPrice);
        product.setProductType(productType.toString());
        product.setSlot(slot);
        product.setProductStatus(ProductStatus.IN_STOCK);
        var savedProduct = productRepository.save(product);
        slot.addProduct(savedProduct);
        slotRepository.save(slot);

        return ResponseEntity.ok(String.format("Product added to slot %d of type %s", slot.getSlotNumber(), slot.getProductType().name()));
    }



//    public ResponseEntity<String> addProductToSlot(String productName, ProductType productType, int slotNumber, Double productPrice, int productQuantity) {
//
//        Slot slot = slotRepository.findBySlotNumber(slotNumber);
//        if (slot == null) {
//            return ResponseEntity.badRequest().body("Invalid slot number");
//        }
//
//        if (slot.getProductType() != productType) {
//            return ResponseEntity.badRequest().body("Invalid product type for this slot");
//        }
//
//        if (slot.isFull()) {
//            List<Slot> availableSlotsOfGivenType = slotRepository.findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(productType);
//            if (availableSlotsOfGivenType.isEmpty()) {
//                List<Slot> availableSlot = slotRepository.findFirstByProductTypeAndSlotOccupancyLessThanMaxCapacity(
//                        productType == ProductType.COOKIE ? productType.CHIPS : ProductType.COOKIE
//                );
//                var slotAny = availableSlot.stream().findFirst();
//                if (slotAny.isPresent()) {
//                    slot = slotAny.get();
//                } else {
//                    return ResponseEntity.badRequest().body("All slots for this product type are full");
//                }
//            } else {
//                var slotOfSameType = availableSlotsOfGivenType.stream().map(Slot::getSlotNumber).collect(Collectors.toList());
//
//                return ResponseEntity.badRequest().body("Please Add to Other Available Slot of Same Product types which are " + slotOfSameType);
//            }
//
//        }
//        int totalStock = slot.getSlotOccupancy() + productQuantity;
//        if (totalStock > 10) {
//            int i = 10 - slot.getSlotOccupancy();
//            return ResponseEntity.badRequest().body("Available size in a slot is " + i);
//
//        }
//        slot.setSlotOccupancy(totalStock);
//        Product product = new Product();
//        product.setProductName(productName);
//        product.setProductPrice(productPrice);
//        product.setProductType(productType.toString());
//        product.setSlot(slot);
//        var savedProduct = productRepository.save(product);
//        slot.addProduct(savedProduct);
//        slotRepository.save(slot);
//        return ResponseEntity.ok(String.format("Product added to slot %d of type %s", slot.getSlotNumber(), slot.getProductType().name()));
//    }

    public void updateProductStatusWhenSlotOccupancyIsZero(Long slotId) {
        Slot slot = slotRepository.findById(slotId).orElseThrow();
        Long productId = slot.getProductId();
        if (productId != null) {
            Product product = productRepository.findByProductId(productId).orElseThrow();
            product.setProductStatus(ProductStatus.OUT_OF_STOCK);
            productRepository.save(product);
        }
    }

    @Override
    public boolean removeProductFromSlot(Long slotId) {
        Optional<Slot> slotOptional = slotRepository.findById(slotId);
        if (slotOptional.isPresent()) {
            Slot slot = slotOptional.get();
            if (slot.isEmpty()) {
                return false;
            }
            slot.removeProduct();
            if (slot.getSlotOccupancy() == 0 && slot.getProductId() != null) {
                updateProductStatusWhenSlotOccupancyIsZero(slot.getSlotId());
            }
            slotRepository.save(slot);
            return true;
        } else {
            throw new RuntimeException("Slot not found with id " + slotId);
        }
    }
//    @Override
//    public boolean removeProductFromSlot(int slotNumber) {
//        var slot = slotRepository.findBySlotNumber(slotNumber);
//        if (Objects.isNull(slot)) {
//            throw new NoSuchElementException("Slot Not found");
//        }
//        if (slot.getSlotOccupancy() <= 0) {
//            throw new IllegalArgumentException("There is no stock available in provided slot " + slotNumber);
//        }
//        boolean removed = slot.removeProduct();
//        if (!removed) {
//            throw new IllegalArgumentException("Unable to remove product from slot");
//        }
//        slotRepository.save(slot);
//        return true;
//    }

//    @Override
//    public boolean removeProductFromSlot(int slotNumber) {
//        var slot = slotRepository.findBySlotNumber(slotNumber);
//        if (Objects.isNull(slot)) {
//            throw new NoSuchElementException("Slot Not found");
//        }
//        if (slot.getSlotOccupancy() <= 0) {
//            throw new IllegalArgumentException("There is no stock available in provided slot " + slotNumber);
//        }
//        Product product = slot.getProducts().get(0);
//        boolean removed = slot.removeProduct();
//        if (!removed) {
//            throw new IllegalArgumentException("Unable to remove product from slot");
//        }
//        slotRepository.save(slot);
//        // Update the status of the product when all the stock in the productId is finished
//        if (slot.getSlotOccupancy() == 0) {
//            product.setProductStatus(ProductStatus.OUT_OF_STOCK);
//            productRepository.save(product);
//            // Call the updateProductStatusWhenSlotOccupancyIsZero method to automatically update the product status
//            updateProductStatusWhenSlotOccupancyIsZero(product.getProductId());
//        }
//        return true;
//    }


//    @Override
//    public void updateProductStatus(Long productId, ProductStatus productStatus) {
//        productRepository.updateProductStatus(productId, productStatus);
//    }

//    @Transactional
//    public void updateProductStatusWhenSlotOccupancyIsZero(Long productId) {
//        // Find the slot that contains the given product ID
//        Slot slot = slotRepository.findByProductId(productId);
//
//        // Check if the slot occupancy is zero
//        if (slot.getSlotOccupancy() == 0) {
//            // Update the product status to out of stock
//            productRepository.updateProductStatus(productId, ProductStatus.OUT_OF_STOCK);
//        }
//    }


    @Override
    public int getStock(int slotNumber) {
        return slotRepository.findBySlotNumber(slotNumber).getSlotOccupancy();
    }

    @Override
    public List<Integer> getAvailableSlots() {
        return slotRepository.findAll().stream().filter(slot -> !slot.isFull()).map(Slot::getSlotNumber).collect(Collectors.toList());

    }

    @Override
    public List<Slot> getAllSlots() {
        return slotRepository.findAll();
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public ResponseEntity<Slot> getSlot(int slotNumber) {
        var slot = slotRepository.findBySlotNumber(slotNumber);
        if (Objects.isNull(slot)) {
            new NoSuchElementException("Slot Not found");

        }

        return ResponseEntity.ok(slot);
    }

    @Override
    public ResponseEntity<String> createTransaction(Long productId, int productQuantity, BigDecimal paymentPrice) {
        var product = productRepository.findById(productId).orElseThrow(() -> new IllegalArgumentException("Product Not Found"));
        if (productQuantity <= 0) {
            throw new IllegalArgumentException("Product quantity should be greater than 0");
        }
        if (product.getSlot().getSlotOccupancy() < productQuantity) {
            throw new NoSuchElementException("Available stock is less than order stock");
        }
        BigDecimal totalPrice = BigDecimal.valueOf(product.getProductPrice() * productQuantity);
        if (totalPrice.compareTo(paymentPrice) > 0) {
            throw new IllegalArgumentException("Total Payment of your products is " + totalPrice + " please enter correct amount");
        }

        Transaction transaction = new Transaction();
        transaction.setTransactionTime(new Date());
        transaction.setAmount(totalPrice);
        transaction.setProductName(product.getProductName());
        transaction.setProductQuantity(productQuantity);
        var slot = product.getSlot();
        int slotStock = slot.getSlotOccupancy() - productQuantity;
        slot.setSlotOccupancy(slotStock);
        product.setSlot(slot);
        if (slotStock == 0) {
            product.setProductStatus(ProductStatus.OUT_OF_STOCK);
        }
        transaction.setSlot(slot);
        slotRepository.save(slot);
        productRepository.save(product);
        transactionRepository.save(transaction);
        BigDecimal difference = paymentPrice.subtract(totalPrice);
        return ResponseEntity.ok().body("Payment successfully done with id " + transaction.getTransactionId() + " your returned amount is " + difference);
    }


//    @Override
//    public ResponseEntity<String> createTransaction(Long productId, int productQuantity, BigDecimal paymentPrice) {
//        var product = productRepository.findById(productId).orElseThrow(() -> new IllegalArgumentException("Product Not Found"));
//        if (productQuantity <= 0) {
//            throw new IllegalArgumentException("Product quantity should be greater than 0");
//        }
//        if (product.getSlot().getSlotOccupancy() < productQuantity) {
//            throw new NoSuchElementException("Available stock is less than order stock");
//        }
//        BigDecimal totalPrice = BigDecimal.valueOf(product.getProductPrice() * productQuantity);
//        if (totalPrice.compareTo(paymentPrice) > 0) {
//            throw new IllegalArgumentException("Total Payment of your products is " + totalPrice + " please enter correct amount");
//        }
//
//        Transaction transaction = new Transaction();
//        transaction.setTransactionTime(new Date());
//        transaction.setAmount(totalPrice);
//        transaction.setProductName(product.getProductName());
//        transaction.setProductQuantity(productQuantity);
//        var slot = product.getSlot();
//        int slotStock = slot.getSlotOccupancy() - productQuantity;
//        slot.setSlotOccupancy(slotStock);
//        product.setSlot(slot);
//        transaction.setSlot(slot);
//        slotRepository.save(slot);
//        productRepository.save(product);
//        transactionRepository.save(transaction);
//        BigDecimal difference = paymentPrice.subtract(totalPrice);
//        return ResponseEntity.ok().body("Payment successfully done with id " + transaction.getTransactionId() + " your returned amount is " + difference);
//    }

    @Override
    public List<Transaction> getTransactionList() {
        return transactionRepository.findAll();
    }

    @Override
    public boolean removeProductById(Long productId) {
        var product = productRepository.findByProductId(productId);
        if (product.isEmpty()) {
            throw new NoSuchElementException("Product not found");
        }

//        // Get the slot associated with the product
//        var slot = slotRepository.findByProductId(productId);
//        if (slot.isEmpty()) {
//            throw new NoSuchElementException("Slot not found for product");
//        }
//
//        // Update the slot to indicate that it is now available
//        slot.get().setProduct(null);
//        slot.get().setQuantity(0);
//        slotRepository.save(slot.get());

        // Delete the product from the repository
        productRepository.delete(product.get());

        return true;
    }


}